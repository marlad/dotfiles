# .bash_aliases

alias a='alias'
alias cal='ncal -M -b'
alias ch='clear;history -c;history -w'
alias ls='ls -F'
alias ll='ls -lhF'
alias less='less -FiX'
alias motd='source $HOME/bin/$(uname -s)/motd'
alias tmux='tmux -2'
