#!/bin/bash

set -eu

PWD=`pwd`
DIRNAME=`dirname $0`
DIR=${PWD}/${DIRNAME}

function linkdotfile {
  file="$1"
  if [ ! -e ~/.$file -a ! -L ~/.$file ]; then
      echo "~/.$file not found, linking..." >&2
      ln -s $DIR/$file ~/.$file
  else
      echo "~/.$file found, ignoring..." >&2
  fi
}

function linkdotconfigfile {
  file="$1"
  if [ ! -e ~/.config/$file -a ! -L ~/.config/$file ]; then
      echo "~/.config/$file not found, linking..." >&2
      ln -s $DIR/config/$file ~/.config/$file
  else
      echo "~/.config/$file found, ignoring..." >&2
  fi
}

for ii in `ls -1 ${DIR} | grep -Ev '(install|README)'`
do
    if [ ! "${ii}" == "config" ]; then
        linkdotfile $ii
    fi
done

for ii in `ls -1 ${DIR}/config/`
do
    linkdotconfigfile $ii
done

cd $DIR
git submodule sync
git submodule update --init --recursive
cd -
