" .vimrc
" see also: http://vim.wikia.com/wiki/Vim_Tips_Wiki
"
" mkdir -p ~/.vim/autoload
" curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
" git clone https://github.com/vim-syntastic/syntastic.git ~/.vim/bundle/syntastic
" git clone https://github.com/vim-airline/vim-airline.git ~/.vim/bundle/vim-airline
" git clone https://github.com/vim-airline/vim-airline-themes.git ~/.vim/bundle/vim-airline-themes
" git clone https://github.com/ntpeters/vim-better-whitespace.git ~/.vim/bundle/vim-better-whitespace

set nocompatible                                " don't emulate old bugs and set modeline.
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set background=dark
" set paste                                       " Paste mode on; conflicts with expandtab
set showmatch                                   " show matching brackets
set laststatus=2
set background=dark
set mouse=                                      " Turn off mouse support
set t_Co=256                                    " Enable 256 colors in vim
highlight ColorColumn ctermbg=10                " Colorcolumn in Green
syntax on

" For management of individually installed plugins in ~/.vim/bundle
" https://github.com/tpope/vim-pathogen
execute pathogen#infect()

" Disable & enable /etc/vimrc stuff (After loading pathogen)
filetype off
filetype plugin indent on

" Syntastic is a syntax checking plugin
" https://github.com/vim-syntastic/syntastic
let g:syntastic_always_populate_loc_list = 1    " Syntastic syntax checker
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers=["flake8"]
let g:syntastic_puppet_checkers=["puppet-lint"]

" Airline manages statusline
" https://github.com/vim-airline/vim-airline
let g:airline_theme = 'bubblegum'
let g:airline_powerline_fonts = 0
let g:airline_left_sep = ''
let g:airline_right_sep = ''

" File extention specific settings
autocmd BufNewFile,BufRead *.py,*.pp,*.py.erb,*.go   set tabstop=4 softtabstop=4 shiftwidth=4 expandtab colorcolumn=80
autocmd BufNewFile,BufRead *.pm,*.pl            set tabstop=4 softtabstop=4 shiftwidth=4 expandtab colorcolumn=80
autocmd BufNewFile,BufRead *.md                 set filetype=markdown

" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
          \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline,"")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

let perl_include_pod = 1                        " my perl includes pod
let perl_extended_vars = 1                      " syntax color complex things like @{${"foo"}}

" Correctly detect Puppet manifests
augroup autodetect
  autocmd!
  autocmd BufRead,BufNewFile *.pp set filetype=puppet
augroup END
